// a way to represent animals

// need a dog and a cat
// both dog and cat has 4 legs
// both has fur

// dog can bark, a cat cannot.
// a cat can meow, but a dog cannot.


class Animal {
    constructor(legs, fur){
        this.legs = legs
        this.fur = fur
    }
}

class Dog extends Animal {
    constructor(legs, fur){
        super(legs, fur)
    }

    bark(name){
        return `${name} is a dog, he/she has ${this.legs} legs, ${this.fur} fur and can bark`
    }
}

class Cat extends Animal {
    constructor(legs, fur){
        super(legs, fur)
    }

    meow(name){
        return `${name} is a cat, he/she has ${this.legs} legs, ${this.fur} fur and can meow`
    }
}

const cat = new Cat(4, 'black');
const dog = new Dog(4, 'white');

console.log(dog.bark('Charlie'));
console.log(cat.meow('Molly'));