var express = require('express');
var router = express.Router();

var getMax = require('../exercise1')

console.log(getMax)

router.get('/', function(req, res, next) {

    // get the data from the user and do something do it.
    let numbers = req.query.num.split(',');
    // convert string to integer.
    numbers = numbers.map(el => +el);

    console.log('Web input =>', numbers);

    // get the max number.
    const max = getMax(numbers);

    console.log('Web result =>', max);

    // respond
    res.send({
        success: true,
        max: max.max, // max number
        secondLargest: max.secondLargest // second largest
    });
});

module.exports = router;