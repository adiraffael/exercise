// get largest, and second largest numbert

const numbers = [ 4894, 24,2 , 0, 29, 498,30, 590]

function getNumbers(numbers) {

  let max = -Infinity, secondLargest = -Infinity;

  for (let i = 0; i <= numbers.length; i++) {
    if (numbers[i] > max) { // check if the number less than maxNumber.
      [secondLargest, max] = [max, numbers[i]]; // if TRUE, change the value of maxNumber and secondLargest.
    } else if (numbers[i] < max && numbers[i] > secondLargest) {  // check if the number less than maxNumber and more than the secondLargest.
      secondLargest = numbers[i]; // if TRUE, change the value of secondLargest.
    }
  }

  return { max, secondLargest }
  
}

console.log(getNumbers(numbers))

module.exports = getNumbers